#!/bin/sh
cd "${0%/*}"
PWD="$(pwd)"
export HOME="${PWD}/linux_extra"
./linux/etl +set fs_homepath "${PWD}/ethome" +set fs_basepath "${PWD}/linux" +seta com_hunkmegs 512 +exec cfg/start/etl_linux +exec cfg/start/screen_archlinux +exec cfg/start/etc "$@"
